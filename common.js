'use strict'

function createNewUser() {
    let newUser = {
        userName: prompt('Enter your name'),
        userSurname: prompt('Enter your surname'),
        userBirthday: prompt("Enter your date of birth", "dd.mm.yyyy"),

        getLogin: function () {
            return this.userName[0].toLowerCase().concat(this.userSurname.toLowerCase());
        },
        getAge: function () {
            let arrBirthday = this.userBirthday.split(".");
            let birthday = new Date(arrBirthday[2], arrBirthday[1] - 1, arrBirthday[0]);
            let today = new Date().getTime();
            let difference = today - birthday;
            return Math.floor(difference / 31557600000);
        },
        getPassword: function () {
            return this.userName[0].toUpperCase().concat(this.userSurname.toLowerCase().concat(this.userBirthday.slice(-4)));
        },
    };
    return newUser;
}

let user = createNewUser();

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());