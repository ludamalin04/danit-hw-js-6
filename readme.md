**1. Екранування** - метод використання символів в тексті, який дозволяє за допомогою символу / прописувати спецсимволи в
якості звичайних символів (розділових знаків чи інших).

**2. Оголошення функції** можна робити декларуванням (function
functionName()) і вираженням через змінну (let functionName = function()).